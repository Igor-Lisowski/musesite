package com.musesite.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.musesite.model.Album;
import com.musesite.model.Band;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BandsSerializer extends StdSerializer<List<Band>> {
    public BandsSerializer() {
        this(null);
    }
    public BandsSerializer(Class<List<Band>> t) {
        super(t);
    }
    @Override
    public void serialize(
            List<Band> bands,
            JsonGenerator generator,
            SerializerProvider provider
    ) throws IOException, JsonProcessingException {
        List<Band> bs = new ArrayList<>();
        for (Band b : bands) {
            b.setAlbums(null);
            bs.add(b);
        }
        generator.writeObject(bs);
    }
}
