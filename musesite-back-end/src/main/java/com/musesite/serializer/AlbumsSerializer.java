package com.musesite.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.musesite.model.Album;
import com.musesite.model.Band;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AlbumsSerializer extends StdSerializer<List<Album>> {
    public AlbumsSerializer() {
        this(null);
    }
    public AlbumsSerializer(Class<List<Album>> t) {
        super(t);
    }
    @Override
    public void serialize(
            List<Album> albums,
            JsonGenerator generator,
            SerializerProvider provider
    ) throws IOException, JsonProcessingException {
        List<Album> as = new ArrayList<>();
        for (Album a : albums) {
            a.setBands(null);
            as.add(a);
        }
        generator.writeObject(as);
    }
}
