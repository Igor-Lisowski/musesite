package com.musesite.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.musesite.model.Musician;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MusiciansSerializer extends StdSerializer<List<Musician>> {
    public MusiciansSerializer() {
        this(null);
    }
    public MusiciansSerializer(Class<List<Musician>> t) {
        super(t);
    }

    @Override
    public void serialize(
            List<Musician> musicians,
            JsonGenerator generator,
            SerializerProvider provider
    ) throws IOException, JsonProcessingException {
        List<Musician> ms = new ArrayList<>();
        for (Musician m : musicians) {
            m.setAlbums(null);
            ms.add(m);
        }
        generator.writeObject(ms);
    }

}
