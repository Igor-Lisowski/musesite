package com.musesite.dao;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.musesite.model.Album;

@Service
public class AlbumService {
	@Autowired
	private AlbumRepository albumRepository;
	
	public void saveAlbum(Album album) {
		this.albumRepository.save(album);
	}
	
	public List<Album> getAllAlbums() {
		return this.albumRepository.findAlbums();
	}
	
	public void updateAlbum(Album album, Long id) {
//		Album updatedAlbum = albumRepository.findById(id).map(
//			album -> {
//				album.setTitle(newAlbum.getTitle());
//				album.setDuration(newAlbum.getDuration());
//				album.setReleaseDate(newAlbum.getReleaseDate());
//				return albumRepository.save(album);
//			}).orElseGet(() -> {
//			newAlbum.setId(id);
//			return albumRepository.save(newAlbum);
//		});
		Album albumToUpdate = albumRepository.findAlbumById(id);
		if(album.getDuration() != null) {
			albumToUpdate.setDuration(album.getDuration());
		}
		if(album.getTitle() != null) {
			albumToUpdate.setTitle(album.getTitle());
		}
		if(album.getReleaseDate() != null) {
			albumToUpdate.setReleaseDate(album.getReleaseDate());
		}
		if(album.getCoverPath() != null) {
			albumToUpdate.setCoverPath(album.getCoverPath());
		}
		if(album.getSpotifyPath() != null) {
			albumToUpdate.setSpotifyPath(album.getSpotifyPath());
		}
		this.albumRepository.save(albumToUpdate);
	}

	public Optional<Album> getAlbumById(Long id) {
		return this.albumRepository.findById(id);
	}

	public void deleteById(Long id) {
		this.albumRepository.deleteById(id);
	}
	
	@PostConstruct
	private void init() {}
}
