package com.musesite.dao;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.musesite.model.Musician;

@Service
public class MusicianService {
	@Autowired
	private MusicianRepository musicianRepository;
	
	public void saveData(Musician musician) {
		this.musicianRepository.save(musician);
	}
	
	public List<Musician> getAllMusicians() {
		return this.musicianRepository.findMusicians();
	}

	public Optional<Musician> getMusicianById(Long id) {
		return musicianRepository.findById(id);
	}

	public void updateMusician(Musician musician, Long id) {
		Musician musicianToUpdate = musicianRepository.findMusicianById(id);
		if (musician.getName() != null) {
			musicianToUpdate.setName(musician.getName());
		}
		if (musician.getSurname() != null) {
			musicianToUpdate.setSurname(musician.getSurname());
		}
		if (musician.getPicturePath() != null) {
			musicianToUpdate.setPicturePath(musician.getPicturePath());
		}
		this.musicianRepository.save(musicianToUpdate);
	}
	
	@PostConstruct
	private void init() {}
}
