package com.musesite.restful;

import com.musesite.dao.AlbumRepository;
import com.musesite.dao.MusicianRepository;
import com.musesite.model.Album;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.MissingResourceException;

@RestController
public class AlbumMusicianController {
    @Autowired
    AlbumRepository albumRepository;
    @Autowired
    MusicianRepository musicianRepository;

    @Transactional
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/musician/{musicianId}/album/{albumId}")
    public List<Album> associate(@PathVariable Long musicianId, @PathVariable Long albumId) {
        Album album = this.albumRepository.findById(albumId).orElseThrow(() -> new MissingResourceException("Album",
                "Album", albumId.toString()));

        return this.musicianRepository.findById(musicianId).map((musician) -> { musician.getAlbums().add(album);
            return this.musicianRepository.save(musician).getAlbums();
        }).orElseThrow(() -> new MissingResourceException("Musician", "Musician", musicianId.toString()));
    }
}
