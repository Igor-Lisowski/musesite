	package com.musesite.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.musesite.dao.MusicianService;
import com.musesite.model.Musician;

import java.util.Optional;

	@RestController
public class MusicianController {
	@Autowired
	MusicianService musicianService;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(path="/musician")
	public ResponseEntity<String> addMusician(@RequestBody Musician musician) {
		musicianService.saveData(musician);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(path="/musician")
	Iterable<Musician> getMusicians() {
		return musicianService.getAllMusicians();
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(path="/musician/{id}")
	Optional<Musician> getMusician(@PathVariable Long id) {
		return musicianService.getMusicianById(id);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PutMapping(path="/musician/{id}")
	ResponseEntity<String> updateMusician(@RequestBody Musician musician, @PathVariable Long id) {
		musicianService.updateMusician(musician, id);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
}
