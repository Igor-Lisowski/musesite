package com.musesite.restful;

import com.musesite.dao.AlbumRepository;
import com.musesite.dao.BandRepository;
import com.musesite.model.Album;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.MissingResourceException;

@RestController
public class AlbumBandController {
    @Autowired
    private AlbumRepository albumRepository;
    @Autowired
    private BandRepository bandRepository;

    @Transactional
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/band/{bandId}/album/{albumId}")
    public List<Album> associate(@PathVariable Long bandId, @PathVariable Long albumId) {
        Album album = this.albumRepository.findById(albumId).orElseThrow(() -> new MissingResourceException("Album",
                "Album", albumId.toString()));

        return this.bandRepository.findById(bandId).map((band) -> { band.getAlbums().add(album);
            return this.bandRepository.save(band).getAlbums();
        }).orElseThrow(() -> new MissingResourceException("Band", "Band", bandId.toString()));
    }
}