package com.musesite.model;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.musesite.serializer.BandsSerializer;
import com.musesite.serializer.MusiciansSerializer;

@Entity
@Table(name="album")
@JsonIdentityInfo(
		generator = ObjectIdGenerators.PropertyGenerator.class,
		property = "id")
public class Album {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="title")
	private String title;
	@ManyToMany(/*cascade=CascadeType.ALL, */targetEntity = Band.class, mappedBy = "albums")
//	@JoinTable(name="album_band", joinColumns = @JoinColumn(name = "musician_id", referencedColumnName = "id"),
//			inverseJoinColumns = @JoinColumn(name = "album_id",
//					referencedColumnName = "id"))
	@JsonSerialize(using = BandsSerializer.class)
	private List<Band> bands;
	@ManyToMany(/*cascade=CascadeType.ALL, */targetEntity = Musician.class, mappedBy = "albums")
//	@JoinTable(name="album_musician", joinColumns = @JoinColumn(name = "musician_id", referencedColumnName = "id"),
//			inverseJoinColumns = @JoinColumn(name = "album_id",
//					referencedColumnName = "id"))
	@JsonSerialize(using = MusiciansSerializer.class)
	private List<Musician> musicians;
	@Embedded
	@Column(name="duration")
	private Duration duration;
	@Column(name="releasedate")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy", timezone="CET")
	private Date releaseDate;
	@Column(name="coverpath")
	private String coverPath;
	@Column(name="spotifypath")
	private String spotifyPath;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Duration getDuration() {
		return duration;
	}
	public void setDuration(Duration duration) {
		this.duration = duration;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public String getCoverPath() {
		return coverPath;
	}
	public void setCoverPath(String coverPath) {
		this.coverPath = coverPath;
	}
	public String getSpotifyPath() {
		return spotifyPath;
	}
	public void setSpotifyPath(String spotifyPath) {
		this.spotifyPath = spotifyPath;
	}
	public List<Band> getBands() {
		return bands;
	}
	public void setBands(List<Band> bands) {
		this.bands = bands;
	}
	public List<Musician> getMusicians() {
		return musicians;
	}
	public void setMusicians(List<Musician> musicians) {
		this.musicians = musicians;
	}
}
