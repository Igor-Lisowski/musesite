package com.musesite.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity(name="Artist")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Artist {
    @Id
    private Long id;
}
