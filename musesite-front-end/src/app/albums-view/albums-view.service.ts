import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { Album } from '../album';

@Injectable()
export class AlbumsViewService {
	constructor(private http: HttpClient) { }

	getAlbums(): Observable<Album[]> {
		return this.http.get<any[]>('http://localhost:8090/album/').pipe(map(albums => albums.map(Album.fromAny)));
	}
}