import { Component, OnInit } from '@angular/core';

import { Album } from '../album';
import { ALBUMS } from '../mock-albums';
import { AlbumsViewService } from './albums-view.service';

import { ActivatedRoute } from '@angular/router';
import { routes } from '../app-routing.module';

@Component({
  selector: 'app-albums-view',
  templateUrl: './albums-view.component.html',
  styleUrls: ['./albums-view.component.css']
})



export class AlbumsViewComponent implements OnInit {
  public albums :Album[] = [];

  constructor(private albumsViewService: AlbumsViewService) { }

  ngOnInit() {
    this.albumsViewService.getAlbums().subscribe(albums => {
      this.albums = albums;
    });
  }
}
