import { Album } from './album';
import { Duration } from './duration';

export class Band {
	id: number;
	name: string;
	picturePath: string;
	albums: Album[];

	constructor(id: number, name: string, picturePath: string, albums: Album[]) {
		this.id = id;
		this.name = name;
		this.picturePath = picturePath;
		this.albums = albums;
	}

	public static fromAny(b: any): Band {
		var albums: Album[] = [];

		for (var i = 0; i < b.albums.length; i++) {
			try {
			console.log(b.albums[i].duration.hours);
			console.log(b.albums[i].duration.minutes);
			console.log(b.albums[i].duration.seconds);
			var duration = new Duration(b.albums[i].duration.hours, b.albums[i].duration.minutes, b.albums[i].duration.seconds);
			albums.push(new Album(b.albums[i].id, b.albums[i].title, duration, Album.deserializeDate(b.albums[i].releaseDate), b.albums[i].coverPath, b.albums[i].spotifyPath, [], []));
			}
			catch {
				
			}
		}

		return new Band(b.id, b.name, b.picturePath, albums);
	}
}