import { Album } from './album';
import { Duration } from './duration';

export class Musician {
	id: number;
	name: string;
	surname: string;
	birthDate: Date;
	picturePath: string;
	albums: Album[];

	constructor(id: number, name: string, surname: string, birthDate: Date, picturePath: string, albums: Album[]) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.birthDate = birthDate;
		this.picturePath = picturePath;
		this.albums = albums;
	}

	public static fromAny(m: any): Musician {
		var albums: Album[] = [];

		for (var i = 0; i < m.albums.length; i++) {
			var duration = new Duration(m.albums[i].duration.hours, m.albums[i].duration.minutes, m.albums[i].duration.seconds);
			albums.push(new Album(m.albums[i].id, m.albums[i].title, duration, Album.deserializeDate(m.albums[i].releaseDate), m.albums[i].coverPath, m.albums[i].spotifyPath, [], []));
		}

		return new Musician(m.id, m.name, m.surname, Musician.deserializeDate(m.birthDate), m.picturePath, albums);
	}

	public static deserializeDate(releaseDate: string): Date {
	    let rd = releaseDate.split("/");
	    var date: Date;

	    return new Date(rd[2] + "-" + rd[1] + "-" + rd[0]);
  	}
}