import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';

import { AlbumsViewComponent } from './albums-view/albums-view.component';
import { ArtistsViewComponent } from './artists-view/artists-view.component';
import { HomeViewComponent } from './home-view/home-view.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AlbumsViewService } from './albums-view/albums-view.service';
import { ArtistsViewService } from './artists-view/artists-view.service';
import { AlbumViewComponent } from './album-view/album-view.component';
import { SafePipe } from './safe.pipe';
import { BandViewComponent } from './band-view/band-view.component';
import { MusicianViewComponent } from './musician-view/musician-view.component';
import { AddAlbumViewComponent } from './add-album-view/add-album-view.component';
import { AddBandViewComponent } from './add-band-view/add-band-view.component';
import { AddMusicianViewComponent } from './add-musician-view/add-musician-view.component';
import { SpotifySearchViewComponent } from './spotify-search-view/spotify-search-view.component';

@NgModule({
  declarations: [
    AppComponent,
    AlbumsViewComponent,
    HomeViewComponent,
    ArtistsViewComponent,
    AlbumViewComponent,
    SafePipe,
    BandViewComponent,
    MusicianViewComponent,
    AddAlbumViewComponent,
    AddBandViewComponent,
    AddMusicianViewComponent,
    SpotifySearchViewComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AlbumsViewService, ArtistsViewService],
  bootstrap: [AppComponent]
})
export class AppModule { }
