import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';
import { Album } from '../album';
import { Musician } from '../musician';
import { Band } from '../band';

@Injectable({
	providedIn: 'root'
})
export class AddAlbumService {
  constructor(private http: HttpClient) {}

	addAlbum(album: Album) {
		let body = JSON.stringify(album, this.replacer);
        let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin' : '*' });
        this.http
        .post("http://localhost:8090/album",
          body, {
            headers: headers
          })
          .subscribe(data => {
                alert('ok');
          }, error => {
              console.log(error);
          });
	}

	replacer(key, value) {
		if (key == "releaseDate") {

			console.log(value);

			var year = value.substr(0, 4); 
			var month = value.substr(5, 2);
			var day = value.substr(8, 2);

      var date = day + "/" + month + "/" + year;
      console.log(date);

			return date;
		}
		if (key == "id") return undefined;
		else if (key == "bands") return undefined;
		else if (key == "musicians") return undefined;
		else return value;
	}

		getBands(): Observable<Band[]> {
	  	return this.http.get<any[]>('http://localhost:8090/band').pipe(map(bands => bands.map(Band.fromAny)));
	    }

	  getMusicians(): Observable<Musician[]> {
		return this.http.get<any[]>('http://localhost:8090/musician').pipe(map(musicians => musicians.map(Musician.fromAny)));	
	  }

  	getAlbums(): Observable<Album[]> {
		return this.http.get<any[]>('http://localhost:8090/album/').pipe(map(albums => albums.map(Album.fromAny)));
	}

  	associateMusicianToAlbum(musicianId, albumId) {
  		let body = "";
        let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin' : '*' });
      var request = 'http://localhost:8090/musician/' + musicianId + '/album/' + albumId;
      console.log(request);
  		this.http.post(request, body, {headers: headers}).toPromise().then(data => {
        console.log(data);
      });
  	}

  	associateBandToAlbum(bandId, albumId) {
  		let body = "";
        let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin' : '*' });
        var request = 'http://localhost:8090/band/' + bandId + '/album/' + albumId;
  		this.http.post(request, body, {headers: headers}).toPromise().then(data => {
        console.log(data);
      });
  	}
}