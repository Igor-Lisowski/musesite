import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AddAlbumService } from './add-album-view.service';
import { Album } from '../album';
import { Band } from '../band';
import { Musician } from '../musician';
import { Duration } from '../duration';

@Component({
  selector: 'app-add-album-view',
  templateUrl: './add-album-view.component.html',
  styleUrls: ['./add-album-view.component.css']
})
export class AddAlbumViewComponent implements OnInit {
	  title;
	  duration;
   	releaseDate;
   	coverPath;
   	spotifyPath;
    artistName;

   	formdata;

   	album: Album;

    public albums = [];
    public bands = [];
    public musicians = [];

  constructor(private addAlbumService: AddAlbumService) { }

  ngOnInit() {
      this.formdata = new FormGroup({
         title: new FormControl(""),
         duration: new FormControl(""),
         releaseDate: new FormControl(""),
         coverPath: new FormControl(""),
         spotifyPath: new FormControl(""),
         artistName: new FormControl("")
      });

      this.addAlbumService.getBands().subscribe(bands => {
      this.bands = bands;
     }, error => {
              console.log(error);
          });

    this.addAlbumService.getMusicians().subscribe(musicians => {
       this.musicians = musicians;
     }, error => {
              console.log(error);
          });
   }
   onClickSubmit(data) {
   	this.title = data.title;
   	this.duration = data.duration;
   	this.releaseDate = data.releaseDate;
   	this.coverPath = data.coverPath;
   	this.spotifyPath = data.spotifyPath;
    this.artistName = data.artistName;

    var bandId = this.getBandId(this.artistName);
    var musicianId = this.getMusicianId(this.artistName);

    if (bandId != -1 || musicianId != -1) {
      this.album = this.serializeAlbum(this.title, this.duration, this.releaseDate, this.coverPath, this.spotifyPath);
      this.addAlbumService.addAlbum(this.album);
    }
    else {
      alert('Artist does not exist');
    }

    setTimeout(() => {
    this.addAlbumService.getAlbums().subscribe(albums => {
      this.albums = albums;

      console.log(this.albums);

      var albumId = this.getAlbumId(this.title);

      if (bandId != -1) {
        this.addAlbumService.associateBandToAlbum(bandId, albumId);
      }
      else if (musicianId != -1) {
        this.addAlbumService.associateMusicianToAlbum(musicianId, albumId);
      }
      else {
        alert('Cannot associate artist to album');
      }
     }, error => {
              console.log(error);
          });
    }, 1000);
   }

   serializeAlbum(title, duration, releaseDate, coverPath, spotifyPath) {
   	var hours = duration.split(':')[0];
   	var minutes = duration.split(':')[1];
   	var seconds = duration.split(':')[2];
   	return new Album(0, title, new Duration(hours, minutes, seconds), Album.deserializeDate(releaseDate), coverPath, spotifyPath, [], []);
   }

   getBandId(artistName) {
     for (var band of this.bands) {
       if (band.name == artistName) {
         return band.id;
       }
     }
     return -1;
   }

   getMusicianId(artistName) {
     for (var musician of this.musicians) {
       var musicianName = musician.name + " " + musician.surname;
       if (musicianName == artistName) {
         return musician.id;
       }
     }
     return -1;
   }

   getAlbumId(albumName) {
     for (var album of this.albums) {
       if (album.title == albumName) {
         return album.id;
       }
     }
     return -1;
   }
}
