import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAlbumViewComponent } from './add-album-view.component';

describe('AddAlbumViewComponent', () => {
  let component: AddAlbumViewComponent;
  let fixture: ComponentFixture<AddAlbumViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAlbumViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAlbumViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
