import { Duration } from './duration';
import { Band } from './band';
import { Musician } from './musician';


import { SafeUrl } from '@angular/platform-browser';
import {Component, SecurityContext} from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser';

export class Album {
	id: number;
	title: string;
	duration: Duration;
	releaseDate: Date;
	coverPath: string;
  spotifyPath: SafeUrl;
	bands: Band[];
	musicians: Musician[];


	constructor(id: number, title: string, duration: Duration, releaseDate: Date, coverPath: string, spotifyPath: string, bands: Band[], musicians: Musician[]) {
		this.duration = duration;
		this.id = id;
		this.title = title;
		this.duration = duration;
		this.releaseDate = releaseDate;
		this.coverPath = coverPath;
    this.spotifyPath = spotifyPath;
		this.bands = bands;
		this.musicians = musicians;
	}

  public static fromAny(a: any): Album {
    var bands: Band[] = [];
    var musicians: Musician[] = [];

    for (var i = 0; i < a.bands.length; i++) {
      bands.push(new Band(a.bands[i].id, a.bands[i].name, a.bands[i].picturePath, []));
    }

    for (var i = 0; i < a.musicians.length; i++) {

      musicians.push(new Musician(a.musicians[i].id, a.musicians[i].name, a.musicians[i].surname, new Date(a.musicians[i].birthDate), a.musicians[i].picturePath, []));
    }

    return new Album(a.id, a.title, a.duration, Album.deserializeDate(a.releaseDate), a.coverPath, a.spotifyPath, bands, musicians);
  }

	public getArtists(): string {
	    let artists: string[];
	    let concatenatedArtists = "";

      let bands = this.getBands();
      let musicians = this.getMusicians();

      artists = bands.concat(musicians);

	    for (let artist of artists) {
	      concatenatedArtists += artist + ", ";
	    }
	    concatenatedArtists = concatenatedArtists.slice(0, -2);

	    return concatenatedArtists;
  }

  public getReleaseDate(): string {
    return this.releaseDate.getUTCDate() + " " + Album.getMonth(this.releaseDate.getMonth()) + " " + this.releaseDate.getFullYear();
  }

  public static deserializeDate(releaseDate: string): Date {
    let rd = releaseDate.split("/");
    var date: Date;

    return new Date(rd[2] + "-" + rd[1] + "-" + rd[0]);
  }

  public static getMonth(releaseDate): string {
    switch(releaseDate) {
      case 0:
      return "Jan";
      case 1:
      return "Feb";
      case 2:
      return "Mar";
      case 3:
      return "Apr";
      case 4:
      return "May";
      case 5:
      return "Jun";
      case 6:
      return "Jul";
      case 7:
      return "Aug";
      case 8:
      return "Sep";
      case 9:
      return "Oct";
      case 10:
      return "Nov";
      case 11:
      return "Dec";
      default:
      throw new Error("wrong month number");
    }
  }

  public getBands(): string[] {
    var bands: string[] = [];
    if (this.bands == null) {
      return bands;
    }

    for (let band of this.bands) {
      bands.push(band.name);
    }

    return bands;
  }

  public getMusicians(): string[] {
    var musicians: string[] = [];
    if (this.musicians == null) {
      return musicians;
    }

    for (let musician of this.musicians) {
      musicians.push(musician.name + " " + musician.surname);
    }

    return musicians;
  }

  public getSpotifyPath() {
    var dom: DomSanitizer;
    return dom.sanitize(SecurityContext.URL, this.spotifyPath);
  }

  public getArtistRouterLink() {
    if (this.bands.length > 0) {
      return "../band-view/" + this.bands[0].id;
    }
    else if (this.musicians.length > 0) {
      return "../musician-view/" + this.musicians[0].id;
    }
    else {
      return "";
    }
  }
}
