import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { routes } from '../app-routing.module';

import { Album } from '../album';
import { Band } from '../band';

import { BandViewService } from './band-view.service';


@Component({
  selector: 'app-band-view',
  templateUrl: './band-view.component.html',
  styleUrls: ['./band-view.component.css'],
  providers: [BandViewService]
})
export class BandViewComponent implements OnInit {
	band : Band;

  constructor(private route: ActivatedRoute, private bandViewService: BandViewService) { }

  ngOnInit() {
  	bandViewService : BandViewService;
  	this.route.params.subscribe(params => {
      this.bandViewService.getBand(params['id']).subscribe(band => { 
      this.band = band;
    })});
  }
}
