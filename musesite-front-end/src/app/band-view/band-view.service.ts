import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { Band } from '../band';

@Injectable()
export class BandViewService {
  constructor(private http: HttpClient) { }

  getBand(id: String): Observable<Band> {
  	return this.http.get<any>('http://localhost:8090/band/' + id).pipe(map(band => Band.fromAny(band)));
  }
}