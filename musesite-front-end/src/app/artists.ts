import { Band } from './band';
import { Musician } from './musician';

export class Artists {
	bands: Band[];
	musicians: Musician[];

	constructor(bands: Band[], musicians: Musician[]) {
		this.bands = bands;
		this.musicians = musicians;
	}
}
