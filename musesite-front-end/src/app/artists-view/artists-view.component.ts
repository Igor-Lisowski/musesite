import { Component, OnInit } from '@angular/core';

import { ArtistsViewService } from './artists-view.service';
import { Artists } from '../artists';
import { Band } from '../band';
import { Musician } from '../musician';

@Component({
	selector: 'app-artists-view',
	templateUrl: './artists-view.component.html',
	styleUrls: ['./artists-view.component.css']
})
export class ArtistsViewComponent implements OnInit {
	public bands = [];
	public musicians = [];

	constructor(private artistsViewService: ArtistsViewService) { }

	ngOnInit() {
		this.artistsViewService.getBands().subscribe(bands => {
			this.bands = bands;
		});

		this.artistsViewService.getMusicians().subscribe(musicians => {
			this.musicians = musicians;
		});
	}
}
