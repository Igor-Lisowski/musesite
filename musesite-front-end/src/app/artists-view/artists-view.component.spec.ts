import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistsViewComponent } from './artists-view.component';

describe('ArtistsViewComponent', () => {
  let component: ArtistsViewComponent;
  let fixture: ComponentFixture<ArtistsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
