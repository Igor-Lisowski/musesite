import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { Artists } from '../artists';
import { Band } from '../band';
import { Musician } from '../musician';

@Injectable()
export class ArtistsViewService {
  constructor(private http: HttpClient) { }

  getBands(): Observable<Band[]> {
  	return this.http.get<any[]>('http://localhost:8090/band').pipe(map(bands => bands.map(Band.fromAny)));
  }

  getMusicians(): Observable<Musician[]> {
	return this.http.get<any[]>('http://localhost:8090/musician').pipe(map(musicians => musicians.map(Musician.fromAny)));	
  }
}