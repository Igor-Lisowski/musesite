import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { Musician } from '../musician';

@Injectable()
export class MusicianViewService {
  constructor(private http: HttpClient) { }

  getMusician(id: String): Observable<Musician> {
  	console.log(id);
  	return this.http.get<any>('http://localhost:8090/musician/' + id).pipe(map(musician => Musician.fromAny(musician)));
  }
}