import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { routes } from '../app-routing.module';

import { Musician } from '../musician';

import { MusicianViewService } from './musician-view.service';

@Component({
  selector: 'app-musician-view',
  templateUrl: './musician-view.component.html',
  styleUrls: ['./musician-view.component.css'],
  providers: [MusicianViewService]
})
export class MusicianViewComponent implements OnInit {
	musician : Musician;

  constructor(private route: ActivatedRoute, private musicianViewService: MusicianViewService) { }

  ngOnInit() {
  	musicianViewService : MusicianViewService;
  	this.route.params.subscribe(params => {
      this.musicianViewService.getMusician(params['id']).subscribe(musician => { 
      this.musician = musician;
    })});
  }
}
