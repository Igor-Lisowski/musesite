import { Component, OnInit } from '@angular/core';
import { AddBandViewService } from './add-band-view.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Band } from '../band';

@Component({
  selector: 'app-add-band-view',
  templateUrl: './add-band-view.component.html',
  styleUrls: ['./add-band-view.component.css']
})
export class AddBandViewComponent implements OnInit {
	name;
	picturePath;

	formdata;

	band: Band;

  constructor(private addBandViewService: AddBandViewService) { }

  ngOnInit() {
  	this.formdata = new FormGroup({
  		name: new FormControl(""),
  		picturePath: new FormControl("")
  	});
  }

  onClickSubmit(data) {
  	this.name = data.name;
  	this.picturePath = data.picturePath;

  	this.band = this.serializeBand(this.name, this.picturePath);

  	this.addBandViewService.addBand(this.band);
  }

  serializeBand(name, picturePath) {
  	return new Band(0, name, picturePath, []);
  }

}
