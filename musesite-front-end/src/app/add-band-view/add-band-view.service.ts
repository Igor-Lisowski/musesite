import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';
import { Album } from '../album';
import { Musician } from '../musician';
import { Band } from '../band';

@Injectable({
	providedIn: 'root'
})
export class AddBandViewService {
  constructor(private http: HttpClient) {}

	addBand(band: Band) {
		let body = JSON.stringify(band, this.replacer);
        let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin' : '*' });
        this.http
        .post("http://localhost:8090/band",
          body, {
            headers: headers
          })
          .subscribe(data => {
                alert('ok');
          }, error => {
              console.log(error);
          });
	}

	replacer(key, value) {
		if (key == "id") return undefined;
		else if (key == "albums") return undefined;
		else return value;
	}
}