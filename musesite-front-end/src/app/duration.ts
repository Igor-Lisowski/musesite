export class Duration {
	hours: number;
	minutes: number;
	seconds: number;
	constructor(hours: number, minutes: number, seconds: number) {
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
	}
}