import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { Album } from '../album';

@Injectable()
export class AlbumViewService {
  constructor(private http: HttpClient) { }

  getAlbum(id: String): Observable<Album> {
  	return this.http.get<any>('http://localhost:8090/album/' + id).pipe(map(album => Album.fromAny(album)));
  }
}