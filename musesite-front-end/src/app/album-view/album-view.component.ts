import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { routes } from '../app-routing.module';

import { Album } from '../album';

import { AlbumViewService } from './album-view.service';


@Component({
  selector: 'app-album-view',
  templateUrl: './album-view.component.html',
  styleUrls: ['./album-view.component.css'],
  providers: [AlbumViewService]
})
export class AlbumViewComponent implements OnInit {
	album : Album;

  constructor(private route: ActivatedRoute, private albumViewService: AlbumViewService) { }

  ngOnInit() {
    albumViewService : AlbumViewService;
  	this.route.params.subscribe(params => {
      this.albumViewService.getAlbum(params['id']).subscribe(album => { 
      this.album = album;
    })});
  }
}
