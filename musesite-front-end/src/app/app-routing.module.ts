import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {AlbumsViewComponent} from './albums-view/albums-view.component';
import {HomeViewComponent} from './home-view/home-view.component';
import {ArtistsViewComponent} from './artists-view/artists-view.component';
import {AlbumViewComponent} from './album-view/album-view.component';
import {BandViewComponent} from './band-view/band-view.component';
import {MusicianViewComponent} from './musician-view/musician-view.component';
import {AddAlbumViewComponent} from './add-album-view/add-album-view.component';
import {AddBandViewComponent} from './add-band-view/add-band-view.component';
import {AddMusicianViewComponent} from './add-musician-view/add-musician-view.component';
import {SpotifySearchViewComponent} from './spotify-search-view/spotify-search-view.component';

export const routes: Routes = [
	{path: '', redirectTo: '/home-view', pathMatch: 'full'},
	{path: 'home-view', component: HomeViewComponent},
	{path: 'albums-view', component: AlbumsViewComponent},
	{path: 'artists-view', component: ArtistsViewComponent},
	{path: 'album-view/:id', component: AlbumViewComponent},
	{path: 'band-view/:id', component: BandViewComponent},
	{path: 'musician-view/:id', component: MusicianViewComponent},
	{path: 'add-album-view', component: AddAlbumViewComponent},
	{path: 'add-band-view', component: AddBandViewComponent},
	{path: 'add-musician-view', component: AddMusicianViewComponent},
	{path: 'spotify-search-view', component: SpotifySearchViewComponent}
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
