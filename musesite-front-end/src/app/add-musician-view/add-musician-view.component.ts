import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Musician } from '../musician';
import { AddMusicianViewService } from './add-musician-view.service';

@Component({
  selector: 'app-add-musician-view',
  templateUrl: './add-musician-view.component.html',
  styleUrls: ['./add-musician-view.component.css']
})
export class AddMusicianViewComponent implements OnInit {
	name;
	surname;
	birthDate;
	picturePath;

	formdata;

	musician: Musician;

  constructor(private addMusicianViewService: AddMusicianViewService) { }

  ngOnInit() {
  	this.formdata = new FormGroup({
  		name: new FormControl(""),
  		surname: new FormControl(""),
  		birthDate: new FormControl(""),
  		picturePath: new FormControl("")
  	});
  }

  onClickSubmit(data) {
  	this.name = data.name;
  	this.surname = data.surname;
  	this.birthDate = data.birthDate;
  	this.picturePath = data.picturePath;

  	this.musician = this.serializeMusician(this.name, this.surname, this.birthDate, this.picturePath);

  	this.addMusicianViewService.addMusician(this.musician);
  }

  serializeMusician(name, surname, birthDate, picturePath) {
  	return new Musician(0, name, surname, Musician.deserializeDate(birthDate), picturePath, []);
  }

}
