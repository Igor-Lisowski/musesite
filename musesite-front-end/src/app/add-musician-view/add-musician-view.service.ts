import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';
import { Album } from '../album';
import { Musician } from '../musician';
import { Band } from '../band';

@Injectable({
	providedIn: 'root'
})
export class AddMusicianViewService {
  constructor(private http: HttpClient) {}

	addMusician(musician: Musician) {
		let body = JSON.stringify(musician, this.replacer);
        let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin' : '*' });
        this.http
        .post("http://localhost:8090/musician",
          body, {
            headers: headers
          })
          .subscribe(data => {
                alert('ok');
          }, error => {
              console.log(error);
          });
	}

	replacer(key, value) {
		if (key == "birthDate") {

			console.log(value);

			var year = value.substr(0, 4); 
			var month = value.substr(5, 2);
			var day = value.substr(8, 2);

      var date = day + "/" + month + "/" + year;
      console.log(date);

			return date;
		}
		if (key == "id") return undefined;
		else if (key == "albums") return undefined;
		else return value;
	}
}