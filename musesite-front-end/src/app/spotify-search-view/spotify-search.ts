import { SpotifyAlbum } from './spotify-album';

export class SpotifySearch {
	spotifyAlbums: SpotifyAlbum[];

	constructor(spotifyAlbums: SpotifyAlbum[]) {
		this.spotifyAlbums = spotifyAlbums;
	}

	public static fromAny(albums: any): SpotifySearch {
		var spotifyAlbums = [];
		for (let album of albums) {
			spotifyAlbums.push(new SpotifyAlbum(album.name, album.artists[0].name, this.serializeDate(album.release_date), album.images[0].url, album.uri));
		}
		return new SpotifySearch(spotifyAlbums);
	}

	public static serializeDate(spotifyDate: string) {
		let day = Number(spotifyDate.split('-')[2]);
		let month = Number(spotifyDate.split('-')[1]) - 1;
		let year = Number(spotifyDate.split('-')[0]);
		var date = new Date();
		date.setDate(day);
		date.setMonth(month);
		date.setFullYear(year);
		return date;
	}
}

