import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpotifySearchViewComponent } from './spotify-search-view.component';

describe('SpotifySearchViewComponent', () => {
  let component: SpotifySearchViewComponent;
  let fixture: ComponentFixture<SpotifySearchViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpotifySearchViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpotifySearchViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
