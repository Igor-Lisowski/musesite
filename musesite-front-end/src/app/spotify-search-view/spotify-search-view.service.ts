import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';
import { SpotifySearch } from './spotify-search';
import { SpotifyAlbum } from './spotify-album';
import { Access } from './access';
import { Musician } from '../musician';
import { Band } from '../band';
import { Album } from '../album';

@Injectable({
	providedIn: 'root'
})
export class SpotifySearchViewService {
	private fs = require('fs');
	constructor(private http: HttpClient) {}

	getAccess() : Observable<Access> {
		let token = this.fs.readFileSync('./token.txt', 'utf8');
		let output : any;
		let headers = new HttpHeaders({ 'Content-Type':'application/x-www-form-urlencoded',
			'Authorization': 'Basic ' + token });
		let body = 'grant_type=client_credentials';

		return this.http.post<any>('https://accounts.spotify.com/api/token', body, {headers: headers}).pipe(map(access => Access.fromAny(access)));
	}

	getSpotifySearch(name: string, accessToken: string): Observable<SpotifySearch> {
		let request = this.parseName(name);
		let headers = new HttpHeaders({'Authorization': 'Bearer ' + accessToken});
		return this.http.get<any>(request, {headers: headers}).pipe(map(spotifySearch => SpotifySearch.fromAny(spotifySearch.albums.items)));
		}

	parseName(name: string) : string {
		return 'https://api.spotify.com/v1/search?q=' + name.replace(/\s/g, "%20") + "&type=album";
	}

	getBands(): Observable<Band[]> {
  		return this.http.get<any[]>('http://localhost:8090/band').pipe(map(bands => bands.map(Band.fromAny)));
  	}

  	getMusicians(): Observable<Musician[]> {
		return this.http.get<any[]>('http://localhost:8090/musician').pipe(map(musicians => musicians.map(Musician.fromAny)));	
  	}

  	addAlbum(album: Album) {
		let body = JSON.stringify(album, this.replacer);
        let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin' : '*' });
        this.http
        .post("http://localhost:8090/album",
          body, {
            headers: headers
          })
          .subscribe(data => {
                alert('ok');
          }, error => {
              console.log(error);
          });
	}

	replacer(key, value) {
		if (key == "releaseDate") {
			var year = value.substr(0, 4); 
			var month = value.substr(5, 2);
			var day = value.substr(8, 2);

      var date = day + "/" + month + "/" + year;
      console.log(date);

			return date;
		}
		if (key == "id") return undefined;
		else if (key == "bands") return undefined;
		else if (key == "musicians") return undefined;
		else return value;
	}

	  	getAlbums(): Observable<Album[]> {
		return this.http.get<any[]>('http://localhost:8090/album/').pipe(map(albums => albums.map(Album.fromAny)));
		}

	  	associateMusicianToAlbum(musicianId, albumId) {
	  		let body = "";
	        let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin' : '*' });
	      var request = 'http://localhost:8090/musician/' + musicianId + '/album/' + albumId;
	      console.log(request);
	  		this.http.post(request, body, {headers: headers}).toPromise().then(data => {
	        console.log(data);
	      });
	  	}

	  	associateBandToAlbum(bandId, albumId) {
	  		let body = "";
	        let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin' : '*' });
	        var request = 'http://localhost:8090/band/' + bandId + '/album/' + albumId;
	  		this.http.post(request, body, {headers: headers}).toPromise().then(data => {
	        console.log(data);
	      });
	  	}
}