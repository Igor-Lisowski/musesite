import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SpotifySearchViewService } from './spotify-search-view.service';
import { Access } from './access';
import { SpotifyAlbum } from './spotify-album';
import { SpotifySearch } from './spotify-search';
import { Musician } from '../musician';
import { Band } from '../band';
import { Album } from '../album';
import { Duration } from '../duration';

@Component({
  selector: 'app-spotify-search-view',
  templateUrl: './spotify-search-view.component.html',
  styleUrls: ['./spotify-search-view.component.css']
})
export class SpotifySearchViewComponent implements OnInit {
	title;

	formdata;

  albums: Album[];
  musicians: Musician[];
  bands: Band[];

	access: Access;
  spotifySearch: SpotifySearch;
  events = [];

  constructor(private spotifySearchViewService: SpotifySearchViewService) { }

  ngOnInit() {
  	this.formdata = new FormGroup({
  		title: new FormControl("")
  	});
    this.spotifySearchViewService.getBands().subscribe(bands => {
      this.bands = bands;
    });
    this.spotifySearchViewService.getMusicians().subscribe(musicians => {
      this.musicians = musicians;
    });
  }

  onClickSubmit(data) {
  	this.title = data.title;

    this.spotifySearchViewService.getAccess().subscribe(access => {
      this.access = access;

      this.spotifySearchViewService.getSpotifySearch(this.title, access.accessToken).subscribe(spotifySearch => {
        this.spotifySearch = spotifySearch;
        // this.addButtonEvents();
      });
    });
  }

  addButtonEvents() {
    for (let album of this.spotifySearch.spotifyAlbums) {
      let button = document.getElementById(album.spotifyURI);
      button.addEventListener("click", (e:Event) => this.addSpotifyAlbum(album.spotifyURI));
    }
  }

  addSpotifyAlbum(spotifyURI: string) {
    var spotifyAlbum: SpotifyAlbum;
    for (let album of this.spotifySearch.spotifyAlbums) {
      if (album.spotifyURI == spotifyURI) {
        spotifyAlbum = album;
        break;
      }
    }
    var musicianId = -1;
    var bandId = -1;

    for (let musician of this.musicians) {
      if (musician.name + ' ' + musician.surname == spotifyAlbum.artistName) {
        musicianId = musician.id;
        break;
      }
    }

    for (let band of this.bands) {
      if (band.name == spotifyAlbum.artistName) {
        bandId = band.id;
        break;
      }
    }

    if (bandId != -1 || musicianId != -1) {
      let album = this.serializeAlbum(spotifyAlbum.title, spotifyAlbum.releaseDate, spotifyAlbum.coverPath, spotifyURI);
      this.spotifySearchViewService.addAlbum(album);
    }
    else {
      alert('Artist does not exist');
      return;
    }

    setTimeout(() => {
      this.spotifySearchViewService.getAlbums().subscribe(albums => {
        this.albums = albums;

        let albumId = this.getAlbumId(spotifyAlbum.title);

        if (bandId != -1) {
          this.spotifySearchViewService.associateBandToAlbum(bandId, albumId);
        }
        else if (musicianId != -1) {
          this.spotifySearchViewService.associateMusicianToAlbum(musicianId, albumId);
        }
        else {
          alert('Cannot associate artist to album');
        }
      });

    }, 1000);
  }

  serializeAlbum(title, releaseDate, coverPath, spotifyURI) { 
    let spotifyId = spotifyURI.split(':')[2];
    let spotifyPath = '<iframe src="https://open.spotify.com/embed/album/' + spotifyId + '" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>';
    return new Album(0, title, new Duration(0, 0, 0), releaseDate, coverPath, spotifyPath, [], []);
  }

  getAlbumId(albumName) {
    for (var album of this.albums) {
      if (album.title == albumName) {
        return album.id;
      }
    }
    return -1;
  }
}
