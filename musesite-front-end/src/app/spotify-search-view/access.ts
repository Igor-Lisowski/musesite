export class Access {
	accessToken: string;
	tokenType: string;
	expiresIn: number;
	scope: string;

	constructor(accessToken: string, tokenType: string, expiresIn: number, scope: string) {
		this.accessToken = accessToken;
		this.tokenType = tokenType;
		this.expiresIn = expiresIn;
		this.scope = scope;	
	}

	public static fromAny(a: any) {
		return new Access(a.access_token, a.token_type, a.expires_in, a.scope);
	}
}