import { SafeUrl } from '@angular/platform-browser';
import {Component, SecurityContext} from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser';

export class SpotifyAlbum {
	title: string;
	artistName: string;
	releaseDate: Date;
	coverPath: string;
	spotifyURI: string;
	constructor(title: string, artistName: string, releaseDate: Date, coverPath: string, spotifyURI: string) {
		this.title = title;
		this.artistName = artistName;
		this.releaseDate = releaseDate;
		this.coverPath = coverPath;
		this.spotifyURI = spotifyURI;
	}

	public getReleaseDate(): string {
    	return this.releaseDate.getUTCDate() + " " + SpotifyAlbum.getMonth(this.releaseDate.getMonth()) + " " + this.releaseDate.getFullYear();
  }
    public static getMonth(releaseDate): string {
    switch(releaseDate) {
      case 0:
      return "Jan";
      case 1:
      return "Feb";
      case 2:
      return "Mar";
      case 3:
      return "Apr";
      case 4:
      return "May";
      case 5:
      return "Jun";
      case 6:
      return "Jul";
      case 7:
      return "Aug";
      case 8:
      return "Sep";
      case 9:
      return "Oct";
      case 10:
      return "Nov";
      case 11:
      return "Dec";
      default:
      throw new Error("wrong month number");
    }
  }
}